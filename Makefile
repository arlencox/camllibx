VERSION = 0.1
LIBNAME = cucaml

.PHONY: all doc clean install uninstall

all:
	ocamlbuild \
	 	-verbose 4 \
		-cflags -noassert \
		$(LIBNAME).cma \
		$(LIBNAME).cmxa

doc:
	ocamlbuild $(LIBNAME).docdir/index.html

clean:
	ocamlbuild -clean

_build/META: META.in Makefile
	sed -e 's:@@VERSION@@:$(VERSION):' META.in > _build/META

install: _build/META _build/$(LIBNAME).cma _build/$(LIBNAME).cmxa
	cd _build; ocamlfind install $(LIBNAME) META *.mli *.cmi *.cmo *.cmx *.o *.a *.cma *.cmxa

uninstall:
	ocamlfind remove $(LIBNAME)
