module type Vtx = sig
  type t
  val compare: t -> t -> int
  val fmt : Format.formatter -> t -> unit
end
  
module Make(V : Vtx) = struct
  type cmd_t =
  (*| Alloc of 'a * 'a*)
    | Equal of V.t * V.t
    | Load of V.t * V.t
    | Store of V.t * V.t
    | AddressOf of V.t * V.t

  module G = HyperGraphId

  type t = (V.t,unit) G.t

  module VM = Mapx.Make(V)

  let add_edge g upd lhs rhs =
    if G.mem_edge g lhs rhs then
      (g,upd)
    else
      let (g, eid) = G.add_edge g lhs rhs () in
      (g, true)
    

  let rec solve i g vid cnstrs =
    let (g,upd) = List.fold_left (fun (g,upd) cnstr ->
      match cnstr with
        | AddressOf(lhs,rhs) ->
            let lhs = VM.find lhs vid in
            let rhs = VM.find rhs vid in
            add_edge g upd [lhs] [rhs]
        | Equal(lhs,rhs) ->
            let pid = VM.find lhs vid in
            let qid = VM.find rhs vid in
            G.fold_succ (fun srcs rid eid (g,upd) ->
              add_edge g upd [pid] rid
            ) g qid (g,upd)
        | Load(lhs,rhs) ->
            let pid = VM.find lhs vid in
            let qid = VM.find rhs vid in
            G.fold_succ (fun srcs rid eid (g,upd) ->
              let rid = List.hd rid in
              G.fold_succ (fun srcs sid eid (g,upd) ->
                add_edge g upd [pid] sid
              ) g rid (g,upd)
            ) g qid (g,upd)
        | Store(lhs,rhs) ->
            let pid = VM.find lhs vid in
            let qid = VM.find rhs vid in
            G.fold_succ (fun srcs rid eid (g,upd) ->
              G.fold_succ (fun srcs sid eid (g,upd) ->
                add_edge g upd rid sid
              ) g qid (g,upd)
            ) g pid (g,upd)
    ) (g,false) cnstrs in
    if upd then
      solve (i+1) g vid cnstrs
    else begin
      Format.printf "//Completed in %d iterations@." i;
      g
    end

  let add_vtx g vid lbl =
    try
      ignore(VM.find lbl vid);
      (g, vid)
    with Not_found ->
      let (g, vid_lbl) = G.add_vertex g lbl in
      let vid = VM.add lbl vid_lbl vid in
      (g, vid)
      

  let solve g cnstrs =
    let (g,vid) = List.fold_left (fun (g,vid) cnstr ->
      match cnstr with
        | AddressOf(lhs,rhs)
        | Equal(lhs,rhs)
        | Load(lhs,rhs)
        | Store(lhs,rhs) ->
            let (g, vid) = add_vtx g vid lhs in
            let (g, vid) = add_vtx g vid rhs in
            (g,vid)
    ) (g,VM.empty) cnstrs in
    solve 0 g vid cnstrs

  let empty = G.empty

  let pp_dot ff g =
    G.pp_dot V.fmt (fun ff e -> ()) ff g
    
end

  
