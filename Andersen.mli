module type Vtx = sig
  type t
  val compare : t -> t -> int
  val fmt : Format.formatter -> t -> unit
end

module Make :
  functor (V : Vtx) ->
    sig
      type cmd_t =
          Equal of V.t * V.t
        | Load of V.t * V.t
        | Store of V.t * V.t
        | AddressOf of V.t * V.t

      module G :
        sig
          type vtx_id = HyperGraphId.vtx_id
          type edge_id = HyperGraphId.edge_id
          type ('vl, 'el) t = ('vl, 'el) HyperGraphId.t
          val empty : ('vl, 'el) t
          val add_edge :
            ('vl, 'el) t ->
            vtx_id list -> vtx_id list -> 'el -> ('vl, 'el) t * edge_id
          val edge_id : ('vl, 'el) t -> vtx_id list -> vtx_id list -> edge_id
          val mem_edge : ('vl, 'el) t -> vtx_id list -> vtx_id list -> bool
          val edge_label : ('vl, 'el) t -> edge_id -> 'el
          val remove_edge :
            ('vl, 'el) t -> vtx_id list -> vtx_id list -> ('vl, 'el) t
          val add_vertex : ('vl, 'el) t -> 'vl -> ('vl, 'el) t * vtx_id
          val vertex_label : ('vl, 'el) t -> vtx_id -> 'vl
          val mem_vertex : ('vl, 'el) t -> vtx_id -> bool
          val remove_vertex : ('vl, 'el) t -> vtx_id -> ('vl, 'el) t
          val edge_count : ('vl, 'el) t -> int
          val vertex_count : ('vl, 'el) t -> int
          val iter_succ :
            (vtx_id list -> vtx_id list -> edge_id -> unit) ->
            ('vl, 'el) t -> vtx_id -> unit
          val fold_succ :
            (vtx_id list -> vtx_id list -> edge_id -> 'a -> 'a) ->
            ('vl, 'el) t -> vtx_id -> 'a -> 'a
          val iter_pred :
            (vtx_id list -> vtx_id list -> edge_id -> unit) ->
            ('vl, 'el) t -> vtx_id -> unit
          val iter_edge :
            (vtx_id list -> vtx_id list -> edge_id -> unit) ->
            ('vl, 'el) t -> unit
          val iter_vertex : (vtx_id -> unit) -> ('vl, 'el) t -> unit
          val pp_dot :
            (Format.formatter -> 'vl -> unit) ->
            (Format.formatter -> 'el -> unit) ->
            Format.formatter -> ('vl, 'el) t -> unit
        end

      type t = (V.t, unit) G.t

      val empty : t

      val solve : t -> cmd_t list -> t 

      val pp_dot : Format.formatter -> t -> unit
    end
